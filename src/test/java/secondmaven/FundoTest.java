package secondmaven;

import org.junit.Assert;
import org.junit.Test;

import main.java.secondmaven.Fundo;

public class FundoTest {

	@Test
	public void deve_retornar_10_se_valor_maior_que_zero() {
		Fundo fundo = new Fundo();
		double valorRetornado = fundo.validarValor(1d);
		Assert.assertEquals("ERRO", 10D, valorRetornado, 0);
	}

}
